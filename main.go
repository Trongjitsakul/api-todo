package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"github.com/noppong-tr/api-todo/auth"
	"github.com/noppong-tr/api-todo/todo"
)

func main() {
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	db.AutoMigrate(&todo.Todo{}) // Auto Migration Database

	gin.SetMode(gin.DebugMode)
	r := gin.Default()

	r.GET("/ping", func(ctx *gin.Context) { // Example API
		ctx.JSON(http.StatusOK, gin.H{
			"messege": "pong",
		})
	})

	r.GET("/tokenz", auth.AccessToken)

	handler := todo.NewTodoHandler(db)
	r.POST("/todos", handler.NewTask)

	r.Run()
}
